import firebase from 'firebase'

const config = {
  apiKey: "AIzaSyAR-9lXILR2gVs9sqjQSKYOqELY5LfMhXA",
  authDomain: "makeup-scheduler.firebaseapp.com",
  databaseURL: "https://makeup-scheduler.firebaseio.com",
  projectId: "makeup-scheduler",
  storageBucket: "makeup-scheduler.appspot.com",
  messagingSenderId: "392759807569"
};
firebase.initializeApp(config);

export default firebase
export const db = firebase.database();
export const auth = firebase.auth();
export const func = firebase.functions();
